import { Injectable } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { NavController } from '@ionic/angular';
import { Register } from '../models/register.model';

import { Storage } from '@ionic/storage-angular';
import { File } from '@ionic-native/file/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  registers: Register[] = [];

  constructor(
    private storage: Storage,
    private navController: NavController,
    private inAppBrowser: InAppBrowser,
    private file: File,
    private emailComposer: EmailComposer
  ) {
    this.storage.create();
    this.loadStorage();

  }

  async loadStorage() {
    this.registers = (await this.storage.get('registros')) || [];
  }

  async saveRegister(format: string, text: string) {
    await this.loadStorage();
    const newRegister = new Register(format, text);
    this.registers.unshift(newRegister);

    this.storage.set('registros', this.registers);
  }

  openRegister(register: Register) {
    const http = 'http';
    const geo = 'geo';
    this.navController.navigateForward('tabs/tab2');

    switch (register.type) {
      case http:
        this.inAppBrowser.create(register.text, '_system');
        break;
      case geo:
        this.navController.navigateForward(`/tabs/tab2/maps/${register.text}`);
        break;
    }
  }

  sendEmail(){
    const temp=[];
    const titles: string ="Tipo,Formato,Creado en , Texto \n";

    temp.push(titles);
    this.registers.forEach(element => {
      const linea =
      `${element.type},${element.format},${element.created},${element.text.replace(',','')}\n`;
      temp.push(linea)
    });

    this.newFisicFile(temp.join(''));
  }
  newFisicFile(text: string) {
    this.file.checkFile(this.file.dataDirectory, 'registers.csv').then(
      exist=> {
        return this.writeInFile(text);
      }
    ).catch(err=> {
      return this.file.createFile(this.file.dataDirectory,'registers.csv',false)
      .then(created=> this.writeInFile(text))
      .catch(err2=> console.log('No se pudo crear el archivo',err2));
    })
  }

  async writeInFile(text: string){
    await this.file.writeExistingFile(this.file.dataDirectory,'registers.csv', text);

    const file = `${this.file.dataDirectory}registers.csv`;
    let email={
      to:`erik.pinya.7e3@itb.cat`,
      //cc 'erik@klk.com',
      //bcc ['juan@klk.com', 'rodrigo@klk.com'],
      attachments:[
        file
      ],
      subject: 'Bckup',
      body: 'aqui tus copias cruck.<strong>Scan App</strong>',
      isHtml:true

    }

    //send text
    this.emailComposer.open(email);
  }



}
